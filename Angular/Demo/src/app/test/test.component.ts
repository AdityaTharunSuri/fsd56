import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id:number;
  name:string;
  age:number;
  address:any;
  hobbies:any;
  constructor(){
    //alert("Constructor invoke...");
    this.id=101;
    this.name="Aditya";
    this.age=21;
    this.address={
      streetNo:56,
      city:'Hyderabad',
      State:'Telangana'};

      this.hobbies=[
    'Sleeping','Swimming','Singing','Dancing'
      ];
  }
  

  
  ngOnInit() {
    //alert("ngOnInit invoke....");
  }

}
