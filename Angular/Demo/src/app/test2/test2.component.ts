import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit{
  person:any;
  constructor(){
this.person={
  id:101,
  name:'Aditya',
  avg:45.45,
  address:{streetNo:56,
  city:'Hyd',
  state:'Telangana'
},
hobbies:['singing','Dancing','Sleeping']

}
  }
  ngOnInit() {
    
    
  }
  buttonsubmit(){
    alert("Button Clicked");
    console.log(this.person);

  }


}
