import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
 

  isUserLoggedIn: boolean;

  constructor(private http:HttpClient) { 
    this.isUserLoggedIn = false;
  }
  getAllCountries(){
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }
  getEmps(){
    return this.http.get('http://localhost:8085/getEmployees')
  }
  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }
  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }
  regsiterEmployee(employee: any): any {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }

}
